import org.gradle.api.JavaVersion

object AppConfig {

    const val applicationId = "com.rmm.startercomposeapp"

    const val compileSdk = 30

    const val minSdk = 23
    const val targetSdk = 31
    const val versionCode = 1
    const val versionName = "1.0"

}

object Java {

    val version = JavaVersion.VERSION_11
    val versionString = toString(version)

    fun toString(version: JavaVersion) = version.toString()

}
